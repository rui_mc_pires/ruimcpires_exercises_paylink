export default class MagecGetterSetter {
  public get value(): string {
    if (this.value) {
      return this.value;
    } else {
      throw new Error("No value was provided!");
    }
  }

  public set value(val: string) {
    if (val) {
      this.value = val;
    } else {
      throw new Error("No value was provided");
    }
  }
}

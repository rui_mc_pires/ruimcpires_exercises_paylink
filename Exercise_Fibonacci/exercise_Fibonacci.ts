let fibonacci: number;

function getFibonacciNumber(fibonacci) {
  if (fibonacci < 2) {
    return fibonacci;
  } else {
    return (
      getFibonacciNumber(fibonacci - 1) + getFibonacciNumber(fibonacci - 2)
    );
  }
}

console.log("Fibonacci 5th number is " + getFibonacciNumber(5));
console.log("Fibonacci 10th number is " + getFibonacciNumber(10));
console.log("Fibonacci 1st number is " + getFibonacciNumber(1));

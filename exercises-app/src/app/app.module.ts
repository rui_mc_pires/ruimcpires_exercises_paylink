import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExerciseCardComponent } from './components/exercise-card/exercise-card.component';
import { FizzBuzzComponent } from './components/fizz-buzz/fizz-buzz.component';
import { FibonacciComponent } from './components/fibonacci/fibonacci.component';
import { GridComponent } from './grid/grid.component';

@NgModule({
  declarations: [
    AppComponent,
    ExerciseCardComponent,
    FizzBuzzComponent,
    FibonacciComponent,
    GridComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

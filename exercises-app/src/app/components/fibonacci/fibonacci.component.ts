import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fibonacci',
  templateUrl: './fibonacci.component.html',
  styleUrls: ['./fibonacci.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class FibonacciComponent implements OnInit {
  fibonacci: number = 0;

  constructor() {}

  ngOnInit(): void {}

  getFibonacciNumber(num: number): any {
    if (num < 2) {
      return num;
    } else {
      return (
        this.getFibonacciNumber(num - 1) + this.getFibonacciNumber(num - 2)
      );
    }
  }

  getOrdinal(fibonacci: number): string {
    let temp = fibonacci.toString();
    let lastLetter = temp.slice(-1);
    let lastDigit = +lastLetter;

    if (lastDigit === 1) {
      return 'st';
    } else if (lastDigit === 2) {
      return 'nd';
    } else if (lastDigit === 0 && fibonacci === 0) {
      return '';
    } else {
      return 'th';
    }
  }
}

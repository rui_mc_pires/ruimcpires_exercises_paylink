import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fizz-buzz',
  templateUrl: './fizz-buzz.component.html',
  styleUrls: ['./fizz-buzz.component.scss'],
})
export class FizzBuzzComponent implements OnInit {
  //@ts-ignore
  numbers: number;

  constructor() {}

  ngOnInit(): void {
    this.numbers;
  }

  checkNumbers(num: number) {
    this.numbers = num;

    if (num % 3 === 0 && num % 5 === 0) {
      return 'FizzBuzz';
    } else if (num % 3 === 0) {
      return 'Fizz  ';
    } else if (num % 5 === 0) {
      return 'Buzz';
    } else {
      return num;
    }
  }
}

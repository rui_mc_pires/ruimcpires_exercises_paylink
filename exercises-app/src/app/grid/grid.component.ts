import { Component, Input, OnInit } from '@angular/core';
import CardsData from './../mockedData/exercisesCardData.json';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit {
  @Input() cardsData: any;

  constructor() {}

  ngOnInit(): void {
    this.cardsData = CardsData;
  }
}

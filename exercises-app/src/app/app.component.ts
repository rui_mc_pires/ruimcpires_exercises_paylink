import { Component } from '@angular/core';

import { ExerciseCardComponent } from './components/exercise-card/exercise-card.component';
import CardsData from './mockedData/exercisesCardData.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'exercises-app';

  cardsData: any[] = [];

  ngOnInit() {
    this.cardsData = CardsData;
  }
}

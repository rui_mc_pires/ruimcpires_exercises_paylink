import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FibonacciComponent } from './components/fibonacci/fibonacci.component';
import { FizzBuzzComponent } from './components/fizz-buzz/fizz-buzz.component';
import { GridComponent } from './grid/grid.component';

const routes: Routes = [
  { path: '', component: GridComponent },
  { path: 'fizzBuzz', component: FizzBuzzComponent },
  { path: 'fibonacci', component: FibonacciComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
